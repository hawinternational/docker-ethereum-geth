#!/bin/sh
set -eu

if ([ ! -d /root/.ethereum/ ]) then
  geth --datadir $DATA_DIR init $GENESIS_JSON
  geth --datadir $DATA_DIR console --exec "personal.newAccount(String())"
fi

exec geth --bootnodes $BOOTNODES --datadir $DATA_DIR --netrestrict $NET_RESTRICT --rpc --rpcaddr $RPC_ADDR --rpcapi $RPC_API "$@"
