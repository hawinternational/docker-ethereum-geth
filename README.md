# ethereum-geth

* Go Ethereum  
https://github.com/ethereum/go-ethereum

## Usage
### run
```bash
$ docker run -p 8545:8545 hawyasunaga/ethereum-geth --mine
$ geth attach http://localhost:8545
```

#### with genesis.json
```bash
$ docker run -v "$(pwd)"/genesis.json:/root/genesis.json hawyasunaga/ethereum-geth    
```
